import { Component, OnInit } from '@angular/core';
import { RandomNumber, RandomNumberInterface } from './decorators/random-number.decorator';
import {
  ClearForm,
  FormDataEntryEvent,
  FormEvent,
  FormEventType,
  FormSync,
  FormSyncInterface,
} from './traits/form-sync.trait';

export interface AppComponent extends RandomNumberInterface {}
export interface AppComponent extends FormSyncInterface {}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
@RandomNumber(400)
@FormSync()
export class AppComponent implements OnInit, RandomNumberInterface {
  title = 'ts-decorators';

  public aggregateData = {};

  public permitStoringForm = false;

  ngOnInit() {
    this.onEvent(this.onFormEvent.bind(this));
    this.initializeFormValues();
  }

  public initializeFormValues() {
    this.aggregateData = {
      band: {},
      user: {}
    };
  }

  public onFormEvent(formEvent: FormEvent) {
    switch (formEvent.type) {

      case FormEventType.DATA_UPDATE:
        this.setSaveButtonState(formEvent as FormDataEntryEvent<any>);
        break;

      case FormEventType.CLEAR:
        this.initializeFormValues();
        this.permitStoringForm = false;
        break;

    }
  }

  public setSaveButtonState(formData: FormDataEntryEvent<any>) {
    if (formData.isValid) {
      const {type, id, ...dataStoreData} = formData;
      this.aggregateData[id] = dataStoreData;
    } else this.aggregateData[formData.id] = {};

    const aggData = Object.values(this.aggregateData);
    this.permitStoringForm = (aggData.reduce(this.determineValidity, true) as boolean);
  }

  public determineValidity(current: boolean, section): boolean {
    return current && !!section.isValid;
  }

  public storeFormData(clickEvent: MouseEvent) {
    const dataObj = {};
    for (const prop in this.aggregateData) {
      if (prop in this.aggregateData) {
        dataObj[prop] = this.aggregateData[prop].formData;
      }
    }
    console.log('storing form data', dataObj);
  }

  @ClearForm()
  public clearFormContent(): boolean {
    return window.confirm('Really Delete?');
  }
}
