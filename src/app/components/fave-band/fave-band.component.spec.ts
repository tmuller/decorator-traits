import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FaveBandComponent } from './fave-band.component';

describe('FaveBandComponent', () => {
  let component: FaveBandComponent;
  let fixture: ComponentFixture<FaveBandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaveBandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaveBandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
