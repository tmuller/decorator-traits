import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LogResult } from '../../decorators/console.decorator';
import { FormDataEntryEvent, FormEvent, FormEventType, FormSync, FormSyncInterface } from '../../traits/form-sync.trait';

export interface FaveBandComponent extends FormSyncInterface {}

export type BandType = {
  name: string;
  email: string;
};


@Component({
  selector: 'app-fave-band',
  templateUrl: './fave-band.component.html',
  styleUrls: ['./fave-band.component.sass']
})
@FormSync()
export class FaveBandComponent implements OnInit {

  public bandForm: FormGroup = this.fb.group({
    name: ['', Validators.required],
    style: ['']
  });

  constructor(public fb: FormBuilder) { }

  ngOnInit() {
    this.onEvent(this.onFormEvent.bind(this));
  }

  public onFormFieldLeave(formEvent) {
    this.updateFormData();
  }

  public updateFormData() {
    const formValue: FormDataEntryEvent<BandType> = {
      type: FormEventType.DATA_UPDATE,
      id: 'band',
      isValid: this.bandForm.valid,
      formData: this.bandForm.getRawValue(),
    };
    this.emitEvent(formValue);
    console.log('bandForm', this.bandForm.valid, this.bandForm.getRawValue());
  }

  @LogResult()
  public onFormEvent(formEvent: FormEvent) {
    // console.log('form event', formEvent);
    switch (formEvent.type) {

      case FormEventType.DATA_UPDATE:
        break;

      case FormEventType.CLEAR:
        console.log('resetting event type form');
        this.bandForm.reset();
        break;

      case FormEventType.SUBMIT:
        break;
    }

    return 'some return value';
  }
}
