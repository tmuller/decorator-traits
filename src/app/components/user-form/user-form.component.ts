import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormDataEntryEvent, FormEvent, FormEventType, FormSync, FormSyncInterface } from '../../traits/form-sync.trait';

export interface UserFormComponent extends FormSyncInterface {}

export type UserType = {
  formId: string;
  name: string;
  email: string;
};


@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.sass']
})
@FormSync()
export class UserFormComponent implements OnInit {

  public userForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', Validators.required],
  });

  constructor(public fb: FormBuilder) { }

  ngOnInit() {
    this.onEvent(this.onFormEvent.bind(this));
  }

  public onFormFieldLeave(formEvent) {
    const formValue: FormDataEntryEvent<UserType> = {
      type: FormEventType.DATA_UPDATE,
      id: 'user',
      isValid: this.userForm.valid,
      formData: this.userForm.getRawValue()
    };
    this.emitEvent(formValue);
    console.log('userForm', this.userForm.valid, this.userForm.getRawValue());
  }

  public onFormEvent(formEvent: FormEvent) {
    console.log('UserFormComponent', formEvent);
    switch (formEvent.type) {
      case FormEventType.DATA_UPDATE:
        break;

      case FormEventType.CLEAR:
        console.log('resetting band form');
        this.userForm.reset();
        break;

      case FormEventType.SUBMIT:
        break;
    }
  }
}
