import { environment } from '../../environments/environment';

export interface LogResultInterface {
  type: string;
}

export function LogResult(type: string = 'log'): MethodDecorator {

  return function(target: any, propertyKey: string, descriptor: PropertyDescriptor) {

    const origMethod = descriptor.value;
    descriptor.value = function(...args) {
      if (environment.permitLogging) console[type](`${ propertyKey }() called with: `, args);
      const result = origMethod.apply(target, args);
      if (environment.permitLogging) console[type](`${ propertyKey }() result: `, result);
      return result;
    };
  };
}
