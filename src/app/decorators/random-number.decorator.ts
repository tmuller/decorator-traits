export interface RandomNumberInterface {
  randomNumber: number;
}
export function RandomNumber(max: number): ClassDecorator {

  // tslint:disable-next-line:only-arrow-functions
  return function(constructor: any) {
    const randomNumber = Math.round(Math.random() * max);
    constructor.prototype.randomNumber = randomNumber;
  };
}
