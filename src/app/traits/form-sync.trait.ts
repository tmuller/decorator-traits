// Calling this construct a trait, as in Scala, it describes an entity
// with self-contained functionality that can be mixed into classes
// using TypeScript decorators

import { Observable, Subject } from 'rxjs';

export interface FormSyncInterface {
  formEventStream: Observable<any>;
  onEvent: (fct) => {};
  emitEvent: (evt) => {};
  clearForm: () => void;
}

export enum FormEventType {
  DATA_UPDATE = 'FORM_DATA_UPDATE',
  CLEAR = 'FORM_CLEAR',
  SUBMIT = 'FORM_SUBMIT'
}

export interface FormEvent {
  type: FormEventType;
}

export interface FormDataEntryEvent<FormData> extends FormEvent {
  id: string;
  isValid: boolean;
  formData?: FormData;
}

export const FormSync: () => ClassDecorator = (function() {

  const eventSubject = new Subject<FormEvent>();

  return function buildingBlockFunctionality(): ClassDecorator {
    return function(constructor: any) {
      const component = constructor.name;
      let eventSubscription;

      constructor.prototype.formEventStream = eventSubject.asObservable();
      constructor.prototype.onEvent = function(rxOnNext: (x: FormDataEntryEvent<any>) => {}) {
        if (!eventSubscription) {
          eventSubscription = constructor.prototype.formEventStream.subscribe(x => {
            rxOnNext(x);
          });
        }
      };
      const sourceOnDestroy = constructor.prototype.ngOnDestroy;
      constructor.prototype.ngOnDestroy = function(...args) {
        if (eventSubscription) {
          eventSubscription.unsubsqqcribe();
          eventSubscription = null;
        }
        // tslint:disable-next-line:no-unused-expression
        !!sourceOnDestroy && sourceOnDestroy.apply(this, args);
      };

      constructor.prototype.emitEvent = function(evt) {
        eventSubject.next(evt);
      };

      constructor.prototype.clearForm = function(formEvent: FormDataEntryEvent<any>) {
        this.emitEvent({type: FormEventType.CLEAR});
      };
    };
  };
}());

export function ClearForm(): MethodDecorator {
  return function( target: any, propKey: string, descriptor: PropertyDescriptor ) {

    const origMethod = descriptor.value;
    descriptor.value = (...parameters) => {
      console.log('target', target);
      console.log('key', propKey);
      console.log('descriptor', descriptor);
      console.log('origMethod', origMethod);
      // console.log('properties', propertyNames);
      // console.log('properties to clear', target[aggregatePropertyName]);

      const result = origMethod.apply(target, parameters);
      if (result === true) target.clearForm();
    };

    return descriptor;
  };
}
